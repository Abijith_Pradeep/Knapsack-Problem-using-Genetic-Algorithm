# Solving Knapsack Problem using Genetic Algorithm

<h3>Knapsack Problem :</h3>
    Given a Knapsack of maximum capacity W. And N items each with its own value and weight.
    Fill the knapsack with items such that the sum of values of all the items is maximum without exceeding the allowed total weight.

